import type { BillCost } from '@/types/BillCost'
import http from './http'

function addBillCost(billcost: BillCost ) {
  const formData = new FormData()
  formData.append('name', billcost.name)
  formData.append('price', billcost.price.toString())
  formData.append('type', JSON.stringify(billcost.type))
  if (billcost.files && billcost.files.length > 0) formData.append('file', billcost.files[0])
  return http.post('/billcosts', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateBillCost(billcost: BillCost & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', billcost.name)
  formData.append('price', billcost.price.toString())
  formData.append('type', JSON.stringify(billcost.type))
  if (billcost.files && billcost.files.length > 0) formData.append('file', billcost.files[0])
  return http.post(`/billcosts/${billcost.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delBillCost(billcost: BillCost) {
  return http.delete(`/billcosts/${billcost.id}`)
}

function getBillCost(id: number) {
  return http.get(`/billcosts/${id}`)
}

function getBillCostsByType(typeId: number) {
  return http.get('/billcosts/type/' + typeId)
}

function getBillCosts() {
  return http.get('/billcosts')
}

export default { addBillCost, updateBillCost, delBillCost, getBillCost, getBillCosts ,getBillCostsByType}
