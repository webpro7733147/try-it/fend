import type { Promotion } from '@/types/Promotion'
import http from './http'
import type { PromotionDetail } from '@/types/PromotionDetail'

function addPromotion(promotion: Promotion & {files:File[]}) {
  const formData = new FormData()
  formData.append('name',promotion.name)
  formData.append('discount',promotion.discount.toString())
  formData.append('start',promotion.start)
  formData.append('end',promotion.end)
  formData.append('status',JSON.stringify(promotion.status))
  formData.append('condition',promotion.condition)
  formData.append('promotionDetail',JSON.stringify(promotion.promotionDetail))
  if(promotion.files && promotion.files.length>0){
    formData.append('file',promotion.files[0])
    console.log(promotion.files[0]);
   }
   console.log(promotion.promotionDetail);
   
  return http.post('/promotions',formData, {headers:{
    "content-Type": "multipart/form-data"
  }})
}

function updatePromotion(promotion: Promotion & {files:File[]}) {
  console.log(promotion);
  
  const formData = new FormData()
  formData.append('name',promotion.name)
  formData.append('discount',promotion.discount.toString())
  formData.append('start',promotion.start)
  formData.append('end',promotion.end)
  formData.append('status',JSON.stringify(promotion.status))
  formData.append('condition',promotion.condition)
  formData.append('promotionDetail',JSON.stringify(promotion.promotionDetail))
  if(promotion.files && promotion.files.length>0){
    formData.append('file',promotion.files[0])
    console.log(promotion.files[0]);
   }
    return http.post(`/promotions/${promotion.id}`,formData, {headers:{
      "content-Type": "multipart/form-data"
    }})
}

function delPromotion(promotion: Promotion) {
  return http.delete(`/promotions/${promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/promotions/${id}`)
}
function getPromotions() {
  return http.get('/promotions')
}
function getPromoDetail(name:string) {
  return http.get(`/promotions/detail/${ name }`)
}

export default { addPromotion, updatePromotion, delPromotion, getPromotion, getPromotions,getPromoDetail }
