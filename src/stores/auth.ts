import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import { useUserStore } from './user'
import router from '@/router'

export const useAuthStore = defineStore('auth', () => {
  const email = ref('')
  const password = ref('')
  const unsuccess = ref(false)
 

  const userStore = useUserStore()
  const currentUser = ref<User | null>(null)

  function login() {
    currentUser.value = userStore.checkAuth(email.value, password.value)
    console.log(currentUser.value)
    if (currentUser.value == null) {
      unsuccess.value = !unsuccess.value
    }
    router.push({ name: 'MainMenu' })
  }

  

  function logout() {
    currentUser.value = null
    email.value = ''
    password.value = ''
  }

  return {
    currentUser,
    email,
    password,
    unsuccess,
    login,
    logout,
  }
    
})
