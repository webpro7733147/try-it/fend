import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { BillCost } from '@/types/BillCost'
import billcostService from '@/services/billcost'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

export const useBillCostStore = defineStore('billCost', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const dialogDelete = ref(false)
  let editedIndex = -1
  const search = ref('')
  let lastIndex = 7
  const initBillCost: BillCost = {
    id: -1,
    typebill: '',
    user: undefined,
    date: '',
    time: '',
    billtotal: 0,
    branch: undefined
  }

  const editedbillCost = ref<BillCost>(JSON.parse(JSON.stringify(initBillCost)))
  const billCosts = ref<BillCost[]>([])

  async function getBillCost(id: number) {
    try {
      loadingStore.doLoad()
      const res = await billcostService.getBillCost(id)
      editedbillCost.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  async function getBillCosts() {
    try {
      const res = await billcostService.getBillCosts()
      billCosts.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  function openDialog() {
    dialog.value = true
    nextTick(() => {
      editedbillCost.value = Object.assign({}, initBillCost)
      editedIndex = -1
    })
  }
  // function save(){
  //   if (editedbillCost.value.id > -1){
  //    Object.assign(billCosts.value[editedIndex], editedbillCost.value)
  //  } else {
  //    editedbillCost.value.id = lastIndex++
  //    billCosts.value.push(editedbillCost.value)
  //   }
  //   closeDialog()
  //  }

  function save() {
    if (editedbillCost.value.id > -1) {
      Object.assign(billCosts.value[editedIndex], editedbillCost.value)
    } else {
      editedbillCost.value.id = lastIndex++
      billCosts.value.unshift(editedbillCost.value)
    }
    closeDialog()
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedbillCost.value = Object.assign({}, initBillCost)
      editedIndex = -1
    })
  }
  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedbillCost.value = Object.assign({}, initBillCost)
    })
  }

  function deleteItem(item: BillCost) {
    editedIndex = billCosts.value.indexOf(item)
    editedbillCost.value = Object.assign({}, item)
    dialogDelete.value = true
  }
  function deleteItemConfirm() {
    // delete item from List
    billCosts.value.splice(editedIndex, 1)
    closeDelete()
  }

  return {
    billCosts,
    openDialog,
    dialog,
    editedbillCost,
    save,
    closeDialog,
    deleteItem,
    closeDelete,
    dialogDelete,
    deleteItemConfirm,
    search,
    getBillCosts,
  }
})
