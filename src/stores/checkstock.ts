import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useStockStore } from './stock'
import { type CheckStock } from '@/types/CheckStock'
import { type Stock } from '@/types/Stock'
import { useAuthStore } from './auth'


export const useCheckStockStore = defineStore('checkstock', () => {
    const stockStore =useStockStore()
    const authStore = useAuthStore()
    const stockItems = stockStore.stockItems
    const date = ref();
    const formattedDate =ref<string>('')
    const textDate = ref()
    const dialogHistory = ref(false)
    const dialogP = ref(false)
    const checkStock =ref<CheckStock> ({
      id: 0,
      createDate: '',
      userId: 0,
      idStock: 0,
      name: '',
      minimum: 0,
      balance: 0,
      unit: '',
      price: 0,
      amount: 0
    })
    const newCheck = ref<CheckStock>()
   
    const checkSTOCK = ref<CheckStock>(JSON.parse(JSON.stringify (checkStock)))
    const checkStocks = ref<CheckStock[]>([])

    let lastIdcheck = checkStocks.value.length+1

    

    function addStock(){
    //   for (let index = 0; index < stockItems.length; index++) {
    //     checkStocks.value[index].stock = stockStore.stockItems[index];
    //   }
      setDate()
      // addStockinCheck()
       checkStock.value.id = lastIdcheck++

       checkStock.value.user = authStore.currentUser!

       checkStock.value.createDate = formattedDate.value
       
       
      // checkSTOCK.value = checkStock.value
       
       textDate.value  = checkStock.value.createDate.split(',')
      //  console.log('Old balance '+newCheck.value.stock);
       checkStocks.value.unshift(checkStock.value)
       console.log('Final Checkstock no s',checkStock.value.stock);
       
       console.log('CheckSTock',checkStocks.value);
     
       
    }
    function logGG(){
      console.log('log ma doo',checkStock.value.stock);
      
    }
    function setDate(){
        date.value = new Date();
        formattedDate.value = date.value.toLocaleString('en-US', {
          day: 'numeric',
          month: 'numeric',
          year: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          second: 'numeric',
          hour12: false, // 24 ชั่วโมง
        });
        textDate.value = ref(formattedDate.value.split(','))
      }
    function clear(){
      
      checkStock.value = {
        id: 0,
        createDate: '',
        userId: 0,
        idStock: 0,
        name: '',
        minimum: 0,
        balance: 0,
        unit: '',
        price: 0,
        amount: 0
      }
     
    }
    // function logIntplus(){
    //   for (let index = 0; index < stockStore.listCheck.length; index++) {
    //      const u =  Number(stockStore.listCheck[index])
        
    //      checkStock.value[index].amount =u
    //   }
      
   
    // }
  return { stockItems,checkStock,checkStocks,dialogHistory,textDate,newCheck,checkSTOCK,dialogP
           ,addStock,logGG }
})
