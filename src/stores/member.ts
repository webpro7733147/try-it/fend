import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useReceiptStore } from './receipt'
export const useMemberStore = defineStore('member', () => {
  const receiptStore = useReceiptStore()
  const dialogDelete = ref(false)
  const dialog = ref(false)
  const form = ref(false)
  const search = ref('')
  const dis = ref()
  let lastId = 11
  let editedIndex = -1
  const initMember: Member = {
    id: -1,
    name: '',
    tel: '',
    point: 0
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initMember)))
  const members = ref<Member[]>([
    { id: 1, name: 'ประสบ อุบัติเหตุ', tel: '1234567890', point: 150 },
    { id: 2, name: 'โชคดี มีเกลื้อน', tel: '1234567891', point: 10 },
    { id: 3, name: 'สายใจ เกาะมหาสนุก', tel: '1234567892', point: 10 },
    { id: 4, name: 'สมศักดิ์ หวังกระแทกคาง', tel: '1234567893', point: 10 },
    { id: 5, name: 'หวังนที จู๋ยืนยง', tel: '1234567894', point: 10 },
    { id: 6, name: 'ณรงค์ นัดใช้ปืน', tel: '1234567895', point: 10 },
    { id: 7, name: 'กันภัย สูญสิ้นภัย', tel: '1234567896', point: 10 },
    { id: 8, name: 'อูโน่ หลาวทอง', tel: '1234567897', point: 10 },
    { id: 9, name: 'อธิป จู๋กระจ่าง', tel: '1234567898', point: 10 },
    { id: 10, name: 'ศักดิพันธ์ ชอบนอนหงาย', tel: '1234567899', point: 10 },
  ])

  const currentMember = ref<Member | null>()

  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
    receiptStore.receipt.member = currentMember.value
  }

  function clearMember() {
    currentMember.value = null
  }

  function addMember() {
    if (editedMember.value.id > -1) {
      Object.assign(members.value[editedIndex], editedMember.value)
    } else {
      editedMember.value.id = lastId++
      members.value.push(editedMember.value)
      currentMember.value = members.value[lastId - 2]
      receiptStore.receipt.member = currentMember.value
    }

    console.log(currentMember.value);
    
    dialog.value = false

  }
  function usePoint(point:number) {
    if (currentMember.value != null && currentMember.value != undefined){
    dis.value = currentMember.value?.point
    currentMember.value.point = currentMember.value.point-(point*10)}
    
  }
  function addPoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined)
      currentMember.value.point = (5 * point) + currentMember.value.point
  }
  function deletePoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined)
      currentMember.value.point = currentMember.value.point - (5 * point)
  }
  function editMember(p: Member) {
    editedIndex = members.value.indexOf(p)
    editedMember.value = Object.assign({}, p)
    dialog.value = true
  }
  function openDialogDelete(p: Member) {
    editedIndex = members.value.indexOf(p)
    editedMember.value = Object.assign({}, p)
    dialogDelete.value = true
  }
  function closeDialog() {
    dialogDelete.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initMember)
      editedIndex = -1
    })
  }
  function deleteMember() {
    console.log(editedIndex);
    members.value.splice(editedIndex, 1)
    dialogDelete.value = false
  }
  function openDialog() {
    dialog.value = true
    nextTick(() => {
      editedMember.value = Object.assign({}, initMember)
      editedIndex = -1
    })

  }
  function resetPoint(){
    currentMember.value!.point = dis.value
  }
  return {
    members, currentMember, dialogDelete, dialog, form, editedMember,resetPoint,search,
    searchMember, clearMember, addMember, usePoint, addPoint, deletePoint, editMember, openDialogDelete, closeDialog, deleteMember, openDialog
  }
})
