import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import productService from '@/services/product'

export const useProductStore = defineStore('product', () => {
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const search = ref('')
  const messageStroe = useMessageStore()
  const loadingStore = useLoadingStore()
  const initProduct: Product & {files:File[]} = {
    name: '',
    price: 0,
    category: '',
    type: '-',
    gsize: '-',
    sweet_level: '-',
    image:'noimage.jpg',
    files:[]
    }
    const editedProduct = ref<Product & {files:File[]} >(JSON.parse(JSON.stringify (initProduct)))
  const products = ref<Product[]>([])

  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])
  async function getProductCategory(){
    products1.value =[]
    products2.value =[]
    products3.value =[]
    try {
      loadingStore.doLoad()
    let res = await productService.getProductsByType("Drink")
    products1.value = res.data
    res = await productService.getProductsByType("Bakery")
    products2.value = res.data
    res = await productService.getProductsByType("Food")
    products3.value = res.data
    loadingStore.finish()
    } catch (error) {
      console.log(error);
      loadingStore.finish()
    }
    }

    async function getProducts() {
      try {
        loadingStore.doLoad()
      const res = await productService.getProducts()
      products.value = res.data
      loadingStore.finish()
      } catch (error) {
        console.log(error);
        loadingStore.finish()
      }
    }
  function openDialog(){
    dialog.value = true
    nextTick(() => {
      clearForm()
    })
    
  }
  function closeDialog(){
    dialog.value = false
    dialogDelete.value = false
    nextTick(() => {
      clearForm()
    })
  }
  async function save(){
    try {
      loadingStore.doLoad()
     const product = editedProduct.value
     if (!product.id) {
       // Add new
       console.log('Post ' + JSON.stringify(product))
       const res = await productService.addProduct(product)
     } else {
       // Update
       console.log('Patch ' + JSON.stringify(product))
       const res = await productService.updateProduct(product)
     }
     await getProducts()
     loadingStore.finish() 
     } catch (error:any) {
       messageStroe.showMessage(error.message)
       loadingStore.finish() 
     }
    closeDialog()
  }
  async function getProduct(id:number){
    try {
        const product = await productService.getProduct(id);
        editedProduct.value = product.data[0]

    } catch (error) {
      console.log(error);
      
    }
    
  }
  async function editProduct(p:number){
    try{
        await getProduct(p)
        dialog.value = true
      
    }catch(e){
      console.log("error");
    }
    
  }
  async function deleteProduct() {
    loadingStore.doLoad()
    const res = await productService.delProduct(editedProduct.value)
    await getProducts()
    loadingStore.finish()
    dialogDelete.value = false
    clearForm()
  }
  async function openDialogDelete(p:Product){
    const product = await productService.getProduct(p.id!);
    editedProduct.value = product.data[0]
    dialogDelete.value = true
}
function clearForm() {
  editedProduct.value = JSON.parse(JSON.stringify(initProduct))
}
  return { products1,products2,products3,products,dialog,dialogDelete,search,
          getProductCategory,editedProduct,getProducts,closeDialog,openDialog,save,editProduct,openDialogDelete,deleteProduct}
})
