import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useProductStore } from './product'
import { useReceiptStore } from './receipt'
import promotionService from '@/services/promotion'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'
import type { PromotionDetail } from '@/types/PromotionDetail'

export const usePromotionStore = defineStore('promotion', () => {
  const dialog = ref(false)
  const dialogType = ref(false)
  const dialogType2 = ref(false)
  const dialogType3 = ref(false)
  const dialogDelete = ref(false)
  const messageStroe = useMessageStore()
  const loadingStore = useLoadingStore()
  const productStore = useProductStore()
  const product = ref()
  const search = ref('')
  const form = ref(false)
  const currentPromotion = ref<Promotion | null>()
  const disMax = ref(0)
  const receiptStore = useReceiptStore()
  
  let editedIndex = -1

  const initPromotion: Promotion & {files:File[]}= {
    name: '',
    condition: '',
    start: '',
    end: '',
    discount: 0,
    promotionDetail:[],
    image:'onimage.jpg',
    files:[]
  }

  const initPromotionDetail: PromotionDetail = {
    promoName: '',
    productName: '',
    productId: 0
  }
  const editedPromotion = ref<Promotion & {files:File[]}>(JSON.parse(JSON.stringify(initPromotion)))
  const editedPromotionDetail = ref<PromotionDetail>(JSON.parse(JSON.stringify(initPromotionDetail)))
  
  const promotions = ref<Promotion[]>([])
  async function getPromotions() {
    try {
      loadingStore.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    
    for(let i=0;i<promotions.value.length;i++){
      const detail:any |PromotionDetail= await promotionService.getPromoDetail(promotions.value[i].name);
      promotions.value[i].promotionDetail = detail.data;
    }
    
    loadingStore.finish()
    } catch (error) {
      console.log(error);
      loadingStore.finish()
    }
  }
  function checkDisMax(d: number,p:Promotion) {
    if (d > disMax.value) {
      disMax.value = d
    currentPromotion.value = p
    receiptStore.receipt.promotion = currentPromotion.value
    }if(disMax.value == 0){
      currentPromotion.value = null
      receiptStore.receipt.promotion = undefined
    }
    receiptStore.receipt.promotionDiscount = disMax.value
    
  }
  function clearPromo() {
    currentPromotion.value = null
  }

  async function deletePromotion() {
    const rse = await promotionService.delPromotion(editedPromotion.value)
    clearForm()
  }

  function closeDialog() {
    dialogDelete.value = false
    nextTick(() => {
      editedPromotion.value = Object.assign({}, initPromotion)
      editedIndex = -1
    })
  }

  async function openDialogDelete(id: number) {
    const res = await promotionService.getPromotion(id)
    editedPromotion.value =res.data
    dialogDelete.value = true
  }

  async function editPromotion(id:number) {
    console.log(id);
    const res = await promotionService.getPromotion(id)
    clearForm()
    const k = res.data
    editedPromotion.value =res.data
    console.log(editedPromotion.value.id);
    
    editedPromotion.value.promotionDetail = new Array(k.promotionDetails.length)
    productStore.getProducts()
    //------------------------------------------
    if(editedPromotion.value.discount<1){
      const res:PromotionDetail = (await promotionService.getPromoDetail(editedPromotion.value.name)).data[0]
      editedPromotionDetail.value.id = res.id
      const p = productStore.products.find(item=>item.name === res.productName )
      if(p && p.id){
        editedPromotionDetail.value.productId = p.id;
        editedPromotionDetail.value.productName = p.name;
        editedPromotionDetail.value.promoName = editedPromotion.value.name
        editedPromotionDetail.value.promotionId = editedPromotion.value.id
        console.log("Detail after: "+editedPromotionDetail.value.productName);
        editedPromotion.value.promotionDetail.push(editedPromotionDetail.value)
        product.value = editedPromotion.value.promotionDetail[0].productName
        editedPromotionDetail.value = JSON.parse(JSON.stringify(initPromotionDetail))
        dialog.value = true
        console.log(editedPromotion.value);
        
    }
      return
    }if(editedPromotion.value.promotionDetail.length>1 && editedPromotion.value.discount >=1){//price
      const res = (await promotionService.getPromoDetail(editedPromotion.value.name))
      const k = res.data
      product.value = []
      for (let index = 0; index < k.length; index++) {
        product.value.push(k[index].productName)
        dialogType2.value = true
      }
      
      return
    }else{//%
      dialogType3.value = true
      console.log("ผิดแล้วค้าบบบ");
      
      product.value = editedPromotion.value.promotionDetail.map((detail: PromotionDetail) => detail.productName).join(', ');
    }
  }

  function addPromotion() {
    dialog.value = true
  }

  async function save(){
    try {
      loadingStore.doLoad()
     const promotion = editedPromotion.value
     if (!promotion.id) {
       // Add new
       console.log('Post ' + JSON.stringify(promotion))
       const res = await promotionService.addPromotion(promotion)
     } else {
       // Update
       console.log('Patch ' + JSON.stringify(promotion))
       const res = await promotionService.updatePromotion(promotion)
     }
     await getPromotions()
     cleardialog()
     loadingStore.finish() 
     } catch (error:any) {
       messageStroe.showMessage(error.message)
       loadingStore.finish() 
     }
    closeDialog()
  }
  function closeDialogAdd() {
    dialog.value = false
    clearForm()
  }
  function checkProductdis(r: ReceiptItem[]) {
    const dis = ref(0)
    promotions.value.sort((a, b) => b.discount - a.discount)
    for (const p of promotions.value) {
      if (p.product != null && p.product != undefined && typeof p.product === "string") {
        for (let i = 0; i < r.length; i++) {
          if (p.product === r[i].product?.name) {
             dis.value += p.discount*r[i].unit
          }
        }
        checkDisMax(dis.value,p)
        dis.value = 0
      }
    }
  }
  function checkProductSetdis(r: ReceiptItem[]) {
    promotions.value.sort((a, b) => b.price - a.price)
    for (const p of promotions.value) {
      if (p.product != null && p.product != undefined) {
        const productListName = ref<string[]>([])
        productListName.value = p.product.split(",")
        const arraySet: number[] = Array(productListName.value.length).fill(0);
        for (let i = 0; i < r.length; i++) {
           for (let j = 0; j < productListName.value.length; j++) {
              if(r[i].product?.name === productListName.value[j]){
                arraySet[j] = r[i].unit
              }
            }
        }
        if(Math.min(...arraySet)!=0){
          checkDisMax(p.discount*Math.min(...arraySet),p)
        }
      }
    }
  }
  function cleardialog(){
    dialog.value = false
    dialogType.value = false
    dialogType2.value = false
    dialogType3.value = false
    dialogDelete.value = false
    clearForm()
    product.value = ''
  }
  function clearForm(){
    editedPromotion.value = JSON.parse(JSON.stringify(initPromotion))
    editedPromotionDetail.value = JSON.parse(JSON.stringify(initPromotionDetail))
  }
  return {
    promotions,
    currentPromotion,
    disMax,
    dialog,
    dialogDelete,
    form,
    editedPromotion,
    dialogType,
    dialogType2,
    dialogType3,
    product,
    editedIndex,
    search,
    clearPromo,
    deletePromotion,
    closeDialog,
    openDialogDelete,
    editPromotion,
    addPromotion,
    save,
    checkProductdis,
    checkProductSetdis,
    cleardialog,
    getPromotions,
    initPromotionDetail,
    editedPromotionDetail
  }
})
