import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'

import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { usePromotionStore } from './promotion'



export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const promotionStore = usePromotionStore()
  const receiptDialog = ref(false)
  const disMember = ref(0)
  const incomeYear = ref(0)
  const incomeMonth = ref(0)
  const incomeDay = ref(0)
  const activeComponent = ref(1)
  const date = ref();
  const formattedDate =ref<string>('')
  
  
  const textDate = ref()
  let unit = 0
  
  const initReceipt:Receipt={
    id: -1,
    createdDate: '',
    totalBefore: 0,
    memberDiscount: 0,
    promotionDiscount: 0,
    promotion: undefined,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    user: undefined,
    userId: authStore.currentUser?.id,
    memberId: 0,
    member:undefined
  }
  const receipt = ref<Receipt>(JSON.parse(JSON.stringify (initReceipt)))
  const receiptItems = ref<ReceiptItem[]>([])

  const unitItem =ref(1)

  const receipts = ref<Receipt[]>([{
    id: 1,
    createdDate: '1/01/2024, 11:11:11',
    totalBefore: 135,
    memberDiscount: 0,
    promotionDiscount: 13.5,
    promotion: {
      id: 1,
      name: 'มาคนเดียวทำไมซื้อ3แก้วอะ',
      con: 'ถ้าซื้อครบ 3 แก้ว ลดราคาทั้งหมด 10%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.1,
      unit: 3,
      price: 0
    },
    total: 121.5,
    receivedAmount: 121.5,
    change: 0,
    paymentType: 'cash',
    user: {id: 5, email: 'fahrakpor@gmail.com', password:'12345678',fullname: 'Pattarapon Sudprasert', gender:'male', role:'staff'},
    userId: 5,
    memberId: 0,
    member:undefined,
    receiptItems:[{
      id: 1,
      name: 'ลาเต้',
      type: 'H',
      gsize: 'S',
      sweet_level: '100%',
      price: 45,
      unit: 2,
      productId: 1,
      product: { id: 1, name: 'ลาเต้', price: 45.00, category:1,type: 'H',gsize:'S' ,sweet_level:'0%'},
    },{
      id: 2,
      name: 'เอสเพรสโซ',
      type: 'H',
      gsize: 'M',
      sweet_level: '0%',
      price: 45,
      unit: 1,
      productId: 2,
      product: { id: 2, name: 'เอสเพรสโซ', price: 45.00,category:1,type: 'H',gsize:'M' ,sweet_level:'0%'},
    }
  ]
  },
  {
    id: 2,
    createdDate: '1/01/2024, 12:11:11',
    totalBefore: 135,
    memberDiscount: 0,
    promotionDiscount: 13.5,
    promotion: {
      id: 1,
      name: 'มาคนเดียวทำไมซื้อ3แก้วอะ',
      con: 'ถ้าซื้อครบ 3 แก้ว ลดราคาทั้งหมด 10%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.1,
      unit: 3,
      price: 0
    },
    total: 121.5,
    receivedAmount: 121.5,
    change: 0,
    paymentType: 'cash',
    user: {id: 5, email: 'fahrakpor@gmail.com', password:'12345678',fullname: 'Pattarapon Sudprasert', gender:'male', role:'staff'},
    userId: 5,
    memberId: 0,
    member:undefined,
    receiptItems:[{
      id: 1,
      name: 'ลาเต้',
      type: 'H',
      gsize: 'S',
      sweet_level: '100%',
      price: 45,
      unit: 2,
      productId: 1,
      product: { id: 1, name: 'ลาเต้', price: 45.00, category:1,type: 'H',gsize:'S' ,sweet_level:'0%'},
    },{
      id: 2,
      name: 'เอสเพรสโซ',
      type: 'H',
      gsize: 'M',
      sweet_level: '0%',
      price: 45,
      unit: 1,
      productId: 2,
      product: { id: 2, name: 'เอสเพรสโซ', price: 45.00,category:1,type: 'H',gsize:'M' ,sweet_level:'0%'},
    }
  ]
  },
  {
    id: 3,
    createdDate: '1/15/2024, 08:11:11',
    totalBefore: 135,
    memberDiscount: 0,
    promotionDiscount: 13.5,
    promotion: {
      id: 1,
      name: 'มาคนเดียวทำไมซื้อ3แก้วอะ',
      con: 'ถ้าซื้อครบ 3 แก้ว ลดราคาทั้งหมด 10%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.1,
      unit: 3,
      price: 0
    },
    total: 121.5,
    receivedAmount: 121.5,
    change: 0,
    paymentType: 'cash',
    user: {id: 5, email: 'fahrakpor@gmail.com', password:'12345678',fullname: 'Pattarapon Sudprasert', gender:'male', role:'staff'},
    userId: 5,
    memberId: 0,
    member:undefined,
    receiptItems:[{
      id: 1,
      name: 'ลาเต้',
      type: 'H',
      gsize: 'S',
      sweet_level: '100%',
      price: 45,
      unit: 2,
      productId: 1,
      product: { id: 1, name: 'ลาเต้', price: 45.00, category:1,type: 'H',gsize:'S' ,sweet_level:'0%'},
    },{
      id: 2,
      name: 'เอสเพรสโซ',
      type: 'H',
      gsize: 'M',
      sweet_level: '0%',
      price: 45,
      unit: 1,
      productId: 2,
      product: { id: 2, name: 'เอสเพรสโซ', price: 45.00,category:1,type: 'H',gsize:'M' ,sweet_level:'0%'},
    }
  ]
  }])
  let lastidReceipt = receipts.value.length+1

  function addReceiptItem(product: Product) {
    console.log(product);
    const index = receiptItems.value.findIndex((item) => item.product?.name === product.name &&
      item.product?.gsize === product.gsize &&
      item.product?.sweet_level === product.sweet_level &&
      item.product?.type === product.type)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
    } else {

      if (product.category != 1) {
        const newReceipt: ReceiptItem = {
          id: -1,
          name: product.name,
          type: '-',
          gsize: '-',
          sweet_level: '-',
          price: product.price,
          unit: 1,
          productId: product.id,
          product: product
        }
        receiptItems.value.push(newReceipt)
        calReceipt()

      } else {
        const newReceipt: ReceiptItem = {
          id: -1,
          name: product.name,
          type: product.type,
          gsize: product.gsize,
          sweet_level: product.sweet_level,
          price: product.price,
          unit: 1,
          productId: product.id,
          product: product
        }
        receiptItems.value.push(newReceipt)
        calReceipt()

      }
    } unit++
  }
  function editReceiptItem(p: Product, r: ReceiptItem, index: number) {
    const newReceipt: ReceiptItem = {
      id: -1,
      name: r.name,
      type: p.type,
      gsize: p.gsize,
      sweet_level: p.sweet_level,
      price: r.price,
      unit: r.unit,
      productId: r.id,
      product: r.product
    }
    receiptItems.value[index] = newReceipt
    calReceipt()
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    unit -= receiptItems.value[index].unit
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    } else {
      unit--
    }
    item.unit--
    calReceipt()
  }
  function calReceipt() {
    promotionStore.disMax = 0
    promotionStore.checkProductdis(receiptItems.value)
    promotionStore.promotionUnit(receiptItems.value)
    promotionStore.checkProductSetdis(receiptItems.value)
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + (item.price * item.unit)
    }
    receipt.value.totalBefore = totalBefore
    if (promotionStore.currentPromotion) {
      totalBefore = totalBefore - receipt.value.promotionDiscount
    }
    if (memberStore.currentMember) {
      receipt.value.total = totalBefore - disMember.value
      receipt.value.memberDiscount = disMember.value
    } else {
      receipt.value.total = totalBefore
    }
  }
  function clear() {
    changeComponent(1)
    receiptItems.value = []
    receipt.value = {
      id: -1,
      createdDate: '',
      totalBefore: 0,
      memberDiscount: 0,
      promotionDiscount: 0,
      promotion: undefined,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.currentUser?.id,
      memberId: 0
    }
    unit = 0
    memberStore.resetPoint()
    memberStore.clearMember()
    promotionStore.clearPromo()
  }

  function showReceiptDialog() {
    setDate()
    receipt.value.receiptItems = receiptItems.value
    receipt.value.createdDate = formattedDate.value
    textDate.value = receipt.value.createdDate.split(',')
    receiptDialog.value = true
  }
  function showReceiptDialog2(item: Receipt) {
    textDate.value = item.createdDate.split(',')
    receipt.value = item
    receiptDialog.value = true
  }
  function setDisMember(dis: number) {
    disMember.value = dis
    calReceipt()
  }
  function addPoint() {
    memberStore.addPoint(unit)
  }
  function deletePoint() {
    memberStore.deletePoint(unit)
  }
  function addReceipt() {
    if(memberStore.currentMember!=undefined){
      receipt.value.member = memberStore.currentMember
    }
    if(promotionStore.currentPromotion!=undefined){
      receipt.value.promotion = promotionStore.currentPromotion
    }
    if(authStore.currentUser !=undefined){
      receipt.value.user = authStore.currentUser
    }
    receipt.value.id = lastidReceipt++
    console.log(receipt.value);
    
    receipts.value.unshift(receipt.value)
    memberStore.currentMember = undefined
    promotionStore.currentPromotion = undefined
  }
  function setDate(){
    date.value = new Date();
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false, // 24 ชั่วโมง
    });
    textDate.value = ref(formattedDate.value.split(','))
  }
  
  function closeDialog(){
    receiptDialog.value = false
    receipt.value = JSON.parse(JSON.stringify (initReceipt))
  }

  function changeComponent(componentNumber:number){
    activeComponent.value = componentNumber
  }
  function setIncome(){
    incomeYear.value = 0
    incomeMonth.value = 0
    incomeDay.value = 0
    for(const j of receipts.value){
      const dateTime = j.createdDate.split(',')
      const date = dateTime[0].split('/')
      if(date[2]==='2024'){
        incomeYear.value+=j.total
      }if(date[0]==='1'){
        incomeMonth.value+=j.total
      }if(date[1]==='15'){
        incomeDay.value+=j.total
      }
    }
  }
  return {
    receiptItems, receipt, receiptDialog, activeComponent, receipts,textDate,lastidReceipt,unitItem,incomeYear,incomeMonth,incomeDay,
    addReceiptItem, removeReceiptItem, inc, dec, calReceipt, editReceiptItem, clear, showReceiptDialog, setDisMember, addPoint, deletePoint, changeComponent, addReceipt, showReceiptDialog2,closeDialog,setIncome
  }
})
