import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import type { Stock } from '@/types/Stock'
import type { ReceiptStockItem } from '@/types/ReceiptStockItem'
import type { ReceiptStock } from '@/types/ReceiptStock'
import { useAuthStore } from './auth'
import { useCheckStockStore } from './checkstock'
import type { CheckStock } from '@/types/CheckStock'

export const useStockStore = defineStore('stock', () => {
const dialogS = ref(false)
const formS = ref(false)
const dialogE =ref(false)
const dialogDelete = ref(false) 
const dialogCheckStock = ref(false) 
const dialogPrint = ref(false) 
const dialogImport = ref(false)
const dialogShowReceipt =ref(false)
const dialogImportPrint = ref(false)
const authStore= useAuthStore()
const textDate = ref()
const dialogRe= ref(false)
const checkStockStore = useCheckStockStore()
const search = ref('')

const stockItems = ref<Stock[]>([
    { id: 1, name: 'ไซรัป', minimum: 10, balance:15,unit:'ขวด', price:225},
    { id: 2, name: 'เมล็ดกาแฟ', minimum: 10, balance:4,unit:'กิโลกรัม', price:400},
    { id: 3, name: 'ผงชาไทย', minimum: 10, balance:6,unit:'กิโลกรัม', price:300},
    { id: 4, name: 'ผงโกโก้', minimum: 10, balance:5,unit:'กิโลกรัม', price:350},
    { id: 5, name: 'ผงชาเขียว', minimum: 10, balance:8,unit:'กิโลกรัม', price:360},
    { id: 6, name: 'นมจืด', minimum: 10, balance:3,unit:'กิโลกรัม', price:120},
    { id: 7, name: 'น้ำเชื่อม', minimum: 10, balance:8,unit:'ลิตร', price:250},
    { id: 8, name: 'วิปปิ่งครีม', minimum: 10, balance:11,unit:'ลิตร', price:150},
    { id: 9, name: 'น้ำผึ้ง', minimum: 10, balance:14,unit:'ลิตร', price:95},
    { id: 10, name: 'ครีมเทียม', minimum: 10, balance:12,unit:'ลิตร', price:120},
    { id: 11, name: 'น้ำตาลคาราเมล', minimum: 10, balance:4,unit:'กิโลกรัม', price:120},
    { id: 12, name: 'ไข่มุก', minimum: 10, balance:6,unit:'กิโลกรัม', price:35},
    { id: 13, name: 'เมล็ดมะขามคั่ว', minimum: 10, balance:10,unit:'กิโลกรัม', price:250},
    { id: 14, name: 'กีวี', minimum: 10, balance:11,unit:'กิโลกรัม', price:150}
    ])
const initSProduct:Stock = {
      id: -1, 
      name: '', 
      minimum:0, 
      balance:0,
      unit:'', 
      price:0
  
    }
const editedSProduct = ref<Stock>(JSON.parse(JSON.stringify (initSProduct)))
const stockLows = ref<Stock[]>([])
const stockChangeB = ref<Stock[]>([])
const stockChangeC = ref<Stock[]>([])
const editedLows = ref<Stock>(JSON.parse(JSON.stringify (initSProduct)))
let editedIndex = -1
let lastIndex = 15
const unitchange =ref(0)
const listCheck:number[]= Array.from({length:stockItems.value.length},()=>0);

const receiptStockS = ref<ReceiptStock[]>([])
const receiptStockItems = ref<ReceiptStockItem[]>([])
const listReceipt:number[]= Array.from({length:stockItems.value.length},()=>0);
const listBalance:number[]= Array.from({length:stockItems.value.length},()=>0);

let lastIdShow = receiptStockS.value.length+1
const date = ref();
const formattedDate =ref<string>('')
const receiptStock = ref<ReceiptStock>({
id: -1,
idReceipt: '',
nameplace: '',
createdDate: '',
totalBefore: 0,
total: 0,
receivedAmount: 0,
paymentType: '',
userId: 0,

}
)




function editSProduct(s:Stock){
         console.log('Open Edit dialog');
          editedIndex = stockItems.value.indexOf(s)
          editedSProduct.value = Object.assign({}, s)
          dialogE.value = true
          formS.value=true
  }
  function addreceiptStock(){
    console.log('Push ');
    // saveShow()
    setDate()
    receiptStock.value.id = lastIdShow++
    receiptStock.value.user = authStore.currentUser!;
    receiptStock.value.createdDate = formattedDate.value
    textDate.value  = receiptStock.value.createdDate.split(',')
    receiptStockS.value.unshift(receiptStock.value)
    console.log("ReceiptS"+receiptStockS.value);
    
    console.log('ReceiptSTOCK '+ receiptStock.value.receiptStockItems);
    console.log('User in re '+ receiptStock.value.user )
 
}

function showReceiptImport(){
  setDate()
  receiptStock.value.createdDate = formattedDate.value
  textDate.value  = receiptStock.value.createdDate.split(',')
  dialogRe.value = !dialogRe.value
}
  
  function deleteSProduct(s:Stock){
        editedIndex = stockItems.value.indexOf(s)
        editedSProduct.value = Object.assign({}, s)
        dialogDelete.value = true
  }
  function  closeDelete () {
          console.log('Closing dialog');
          dialogDelete.value = false
          nextTick(() => {
              editedSProduct.value = Object.assign({}, initSProduct)
          })
        }
     function closeDialog(){
         console.log('Close')
          dialogE.value = !dialogE.value 
          
          nextTick(() => {
            editedSProduct.value = Object.assign({}, initSProduct)
            editedIndex = -1
          })
        }
        function closeDialogT(){
          console.log('Close')
          editedSProduct.value = Object.assign({}, initSProduct)
          editedIndex = -1
          dialogE.value = !dialogE.value 
         
         }

        function closeDialogAdd(){
          dialogS.value = false
       
          nextTick(() => {
            editedSProduct.value = Object.assign({}, initSProduct)
            editedIndex = -1
          })
        }
        function closeCheckDialog(){
          console.log('CloseCheck')
          dialogCheckStock.value = !dialogCheckStock.value
          nextTick(() => {
            editedLows.value = Object.assign({}, initSProduct)
            editedIndex = -1
          })
        }
  function  deleteItemConfirm () {
        console.log('Delete item');
          // delete item from List
          stockItems.value.splice(editedIndex, 1)
          closeDelete()
        }
       
        function save(){
          
          if (editedSProduct.value.id > -1) {

            Object.assign(stockItems.value[editedIndex], editedSProduct.value)
          
            closeDialog()

          } else {
            editedSProduct.value.id = lastIndex++
            stockItems.value.push(editedSProduct.value)
            closeDialogAdd()
          }
          
        }

    
      
         
        function openAddDialog (){
            dialogS.value = !dialogS.value
        }   
      function minimumStock(){
        stockLows.value =[]
        for(let index = 0; index < stockItems.value.length; index++){
          if(stockItems.value[index].balance<stockItems.value[index].minimum){
            stockLows.value.push(stockItems.value[index])
          }
        
        } 
      }

      function setDate(){
        date.value = new Date();
        formattedDate.value = date.value.toLocaleString('en-US', {
          day: 'numeric',
          month: 'numeric',
          year: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          second: 'numeric',
          hour12: false, // 24 ชั่วโมง
        });
        textDate.value = ref(formattedDate.value.split(','))
      }
     
      function addBalance(){
        console.log('Add balance');
        for (let index = 0; index < listReceipt.length; index++) {
          const u =  Number(listReceipt[index])
          stockItems.value[index].balance =u
      }
      
  }

    function editCheck(s:Stock){

       editedIndex = stockItems.value.indexOf(s)
       editedSProduct.value = Object.assign({}, s)
}
    function saveCheck(){
      console.log('Closed man');
      Object.assign(stockItems.value[editedIndex], editedSProduct.value)
      closeCheckDialog()
      
    }
    function logStupid(){
      console.log(listCheck);
      
    }
    function logIntplus(){
  
      checkStockStore.checkStock.stock = stockChangeC.value
      console.log('Stock in check',checkStockStore.checkStock.stock);
      // BtoC(checkStockStore.checkStock)
       console.log('CheckSITEMS ',checkStockStore.checkStock.stock);
      checkStockStore.addStock()
      // addStockinC()
      addB()
      
    }
    function addStockinC(){
      for (let index = 0; index <  checkStockStore.checkStocks.length; index++) {
        checkStockStore.checkStocks[index].idStock =   checkStockStore.checkStocks[index].stock![index].id
        checkStockStore.checkStocks[index].minimum =   checkStockStore.checkStocks[index].stock![index].minimum
         checkStockStore.checkStocks[index].name =   checkStockStore.checkStocks[index].stock![index].name
          checkStockStore.checkStocks[index].price =   checkStockStore.checkStocks[index].stock![index].price
          checkStockStore.checkStocks[index].unit =   checkStockStore.checkStocks[index].stock![index].unit
      }
       console.log('Function addStock',checkStockStore.checkStocks)
    }
    function BtoC(c:CheckStock){
      for (let index = 0; index < listBalance.length; index++) {
       listBalance[index] = stockChangeB.value[index].balance
       c.stock![index].balance = Number(listBalance[index])
      }
      console.log('Add to c',c.stock!);
      
   
    }
    function newStocks(){
      const updatedStockItems = JSON.parse(JSON.stringify(stockItems.value));
      stockChangeC.value =[]
      stockChangeC.value.push(updatedStockItems)
      console.log('ChangeC',stockChangeC);
    }

    function addB(){
      for(let index = 0; index <listBalance.length; index++){
         const u =  Number(listCheck[index])
         stockItems.value[index].balance =u
      }
     
    }

    function openPrint(){
           dialogCheckStock.value =!dialogCheckStock.value
           dialogPrint.value =!dialogPrint.value
    }
    function openCheckDialog(){
      newStocks()
      dialogCheckStock.value =!dialogCheckStock.value
}
    function openImportDialog(){
          dialogImport.value = !dialogImport.value
          minimumStock()
      }
    const dialogImportM = ref(false)
function openImportManage(){
  dialogImport.value=!dialogImport.value
    dialogImportM.value =!dialogImportM.value
    
    minimumStock()
}
function addStockItem(s:Stock){
      console.log(s);
      const index = receiptStockItems.value.findIndex((item) => item.stock?.name === s.name && 
                                                        item.stock?.minimum ===s.minimum && 
                                                        item.stock?.price ===s.price &&
                                                        item.stock?.unit ===s.unit  )
    if (index >= 0) {
        receiptStockItems.value[index].unitPerItem++
       
    } else {
        const newStockReceipt: ReceiptStockItem = {
          id:-1,
          name:s.name,
          minimum:s.minimum,
          balance:s.balance,
          unit:s.unit,
          price:s.price,
          totalprice:0,
          unitPerItem:1,
          stockId:s.id,
          beforeprice:0,
          stock:s
      }
      receiptStockItems.value.push(newStockReceipt)
      
      
      calReceipt()
}}
function calReceipt(){
  let total =0
  for(const item of receiptStockItems.value){
    total = total+Number(item.totalprice)
}
receiptStock.value.total = total

}
function enterPrice(s:ReceiptStockItem){
  console.log('Enter',s.beforeprice);
  const index = receiptStockItems.value.findIndex((item) => item.stock?.name === s.name && 
                                                        item.stock?.minimum ===s.minimum && 
                                                        item.stock?.price ===s.price &&
                                                        item.stock?.unit ===s.unit )
  receiptStockItems.value[index].totalprice = s.beforeprice
  calReceipt()
  

 
}
function clearPrice(s:ReceiptStockItem){
  // console.log('Enter',s.totalprice);
  const index = receiptStockItems.value.findIndex((item) => item.stock?.name === s.name && 
                                                        item.stock?.minimum ===s.minimum && 
                                                        item.stock?.price ===s.price &&
                                                        item.stock?.unit ===s.unit )
  receiptStockItems.value[index].totalprice = 0

  
}

function addBTest(s:ReceiptStockItem){
  console.log('Enter Balance',s);
  const index = stockItems.value.findIndex((item) => item.id === s.stockId )
  receiptStock.value.receiptStockItems = receiptStockItems.value
  console.log('Index '+index);
  stockItems.value[index].balance =s.unitPerItem
 
 
 

}

function clear(){
   receiptStockItems.value = []
   receiptStock.value ={
    id: -1,
    idReceipt: '',
    nameplace: '',
    createdDate: '',
    totalBefore: 0,
    total: 0,
    receivedAmount: 0,
    paymentType: '',
    userId: 0
   }
}
function closeImportM(){
  console.log('Close import M');
  dialogImportM.value = !dialogImportM.value
  
}


  return {stockItems,initSProduct,editedSProduct,dialogE,formS,dialogDelete,dialogS,dialogCheckStock,stockLows,unitchange,listCheck,dialogPrint,dialogImport
    ,dialogImportM,receiptStockItems,receiptStock,receiptStockS,dialogShowReceipt,dialogImportPrint,textDate,dialogRe,listBalance,stockChangeB,stockChangeC,search
    ,openImportManage,closeDelete,save,openAddDialog,deleteItemConfirm,deleteSProduct,editSProduct,closeDialogAdd,minimumStock,addBalance,saveCheck,editCheck,logStupid,logIntplus
    ,openPrint,openImportDialog,openCheckDialog,addStockItem,enterPrice,clear,closeImportM,addBTest,addreceiptStock,closeDialog,closeDialogT,
    clearPrice,showReceiptImport,setDate,closeCheckDialog,BtoC,newStocks
      }})
