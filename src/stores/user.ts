import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import type { VForm } from 'vuetify/components';
export const useUserStore = defineStore('user', () => {
    const form = ref(false)
    const refForm = ref<VForm | null>(null)
    const dialog = ref(false)
    const dialogDelete = ref(false)
    const search = ref('')
    const loading = ref(false)
    let editedIndex = -1
    const initilUser: User = {
      id: -1,
      email: '',
      password: '',
      fullname: '',
      gender: 'male',
      role: 'manager'
    }
    const editedUser = ref<User>(JSON.parse(JSON.stringify(initilUser)))
    const users = ref<User[]>([
      {id: 1, email: 'yamyen@gmail.com', password:'12345678',fullname: 'Piyaphon Yamyen', gender:'male', role:'manager'},
      {id: 2, email: 'ping@gmail.com', password:'12345678',fullname: 'Nuttakorn Kongpiromchuen', gender:'male', role:'staff'},
      {id: 3, email: 'bank@gmail.com', password:'12345678',fullname: 'Chawanakorn Naowarat', gender:'male', role:'staff'},
      {id: 4, email: 'bright@gmail.com', password:'12345678',fullname: 'Thunpisid Poonpanang', gender:'male', role:'staff'},
      {id: 5, email: 'fahrakpor@gmail.com', password:'12345678',fullname: 'Pattarapon Sudprasert', gender:'male', role:'staff'},
      {id: 6, email: '1', password:'1',fullname: 'User Test 1', gender:'male', role:'manager'}
    ])
    function openDialog(){
        dialog.value = true
        nextTick(() => {
            editedUser.value = Object.assign({}, initilUser)
          editedIndex = -1
        })
    }
    let lastId = users.value.length + 1
    function onSubmit() {}
    
    function closeDelete() {
      dialogDelete.value = false
      nextTick(() => {
        editedUser.value = Object.assign({}, initilUser)
      })
    }
    
    function deleteItemConfirm() {
      // Delete item from list
      users.value.splice(editedIndex, 1)
      closeDelete()
    }
    function deleteItem(item: User) {
      editedIndex = users.value.indexOf(item)
      editedUser.value = Object.assign({}, item)
      dialogDelete.value = true
      editedIndex = -1
    }
    
    function editItem(item: User) {
      editedIndex = users.value.indexOf(item)
      editedUser.value = Object.assign({}, item)
      dialog.value = true
    }
    function closeDialog() {
      dialog.value = false
      nextTick(() => {
        editedUser.value = Object.assign({}, initilUser)
        editedIndex = -1
      })
    }
    function save() {
      if (editedIndex > -1) { 
        Object.assign(users.value[editedIndex], editedUser.value)
      } else {
        editedUser.value.id = lastId++
        users.value.push(editedUser.value)
      }
      editedIndex = -1
      closeDialog()
    }

    function checkAuth(email: string, password: string): User | null {
      const index = users.value.findIndex((item) => item.email === email)
      if (index < 0) return null
      if (users.value[index].password === password) {
        return users.value[index]
      } else {
        return null
      }
      
    }
  
    
    return {
        form,
        loading,
        onSubmit,
        deleteItemConfirm,
        deleteItem,
        editItem,
        save,
        closeDelete,
        closeDialog,
        editedUser,
        dialogDelete,
        openDialog,
        users,
        search,
        dialog,
        checkAuth
    }
})