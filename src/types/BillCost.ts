import type { Branch } from './Branch'
import type { User } from './User'

type BillCost = {
  id?: number
  typebill: string // 'Electricity bill' | 'Water bill' | 'Rent bill'
  user?:User
  date: string // date
  time: string // date(time)
  billtotal: number
  branch?: Branch
}

export { type BillCost }
