import type { User } from './User'
import type { BillCost } from './BillCost'
type Branch = {
  id: number

  name: string

  Address: string

  tel: string

  manager: User

  billcosts: BillCost
}
export { type Branch }
