import type { Stock } from "./Stock";
import type { User } from "./User";

type CheckStock = {
    id: number;
    createDate: string;
    userId:number
    user?: User;
    stock?:Stock[]
    amount:number;
    idStock: number;
    name: string;
    minimum: number;
    balance: number;
    unit:string;
    price:number;
}

export{type CheckStock }