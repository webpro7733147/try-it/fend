type Product = {
    id?: number;
    name: string;
    price: number;
    category: string; // 1= drink ||2 = dessert(ขนมหวาน) || 3 = bakery
    type:string;
    gsize:string;
    sweet_level:string;
    image?:string;
    
}
function getImageUrl(product: Product) {
    return `http://localhost:3000/images/products/${product.image}`
  }

export{type Product,getImageUrl }