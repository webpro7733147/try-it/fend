import type { Product } from "./Product"
import type { PromotionDetail } from "./PromotionDetail"

type Promotion = {
    id?:number
    name:string
    condition:string
    start:string
    end:string
    status?:boolean
    discount:number
    promotionDetail: PromotionDetail[]
    image:string
}

export type {Promotion}