import type { Product } from "./Product"

type PromotionDetail = {
    id?:number
    promoName:string
    productName:string
    promotionId?:number
    productId:number
}

export type {PromotionDetail}