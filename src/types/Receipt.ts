import type  { Member } from "./Member";
import type { Promotion } from "./Promotion";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

type Receipt = {
    id:number
    createdDate: string
    totalBefore: number
    memberDiscount:number
    promotionDiscount:number
    total:number
    receivedAmount:number
    change:number
    paymentType:string
    userId?:number
    user?:User
    memberId:number
    member?:Member
    promotion?:Promotion
    receiptItems?:ReceiptItem[]

}

export type {Receipt}