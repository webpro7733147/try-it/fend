
import type { ReceiptStockItem } from "./ReceiptStockItem";
import type { User } from "./User";

type ReceiptStock = {
    id:number
    idReceipt:string
    nameplace:string
    createdDate:string
    totalBefore: number
    total:number
    receivedAmount:number
    paymentType:string
    userId:number
    user?:User
    receiptStockItems?:ReceiptStockItem[]

}

export type {ReceiptStock}