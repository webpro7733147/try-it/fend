// type Salarybaht = 25000 | 15000 //manager|staff

import type { User } from "./User"
import type { CheckIN } from "./CheckIN"
// type Status = 'Paid' | 'Not Paid'
type Salary = {
    checkin?: CheckIN
    idSalary: number
    userId: number
    user?: User
    month: string
    salarybaht: number
    status: string
}

export type {Salary}